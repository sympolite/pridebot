import xml.etree.ElementTree as ETree


class DictParseError(Exception):
    pass


class FlagDictionary:
    def __init__(self, file):
        self.file = file
        self.root = ETree.parse(self.file).getroot()
        self.flag_dictionary = {}
        self.parse_dict()
        self.print_keys()

    def parse_dict(self):
        for elem in self.root:
            filename = elem.tag
            keywords = elem.text.split(" ")
            if keywords is None:  # empty keyword list
                raise DictParseError(f'Keyword list for "{filename}" is blank.')
            for k in keywords:
                k_strip = k.strip()
                self.flag_dictionary[k_strip] = filename

    def items(self):
        return self.flag_dictionary.items()

    def keys(self):
        return self.flag_dictionary.keys()

    def values(self):
        return self.flag_dictionary.values()

    def __getitem__(self, key):
        return self.flag_dictionary[key]

    def __setitem__(self, key, value):
        self.flag_dictionary[key] = value

    def __delitem__(self, key):
        self.flag_dictionary.pop(key)

    def __contains__(self, item):
        return item in self.flag_dictionary

    def print_keys(self):
        for key, value in self.items():
            print(f"{key}: {value}")

    def get_default(self, key, default):
        if key not in self.flag_dictionary:
            return default
        return self[key]


class FlagList:
    def __init__(self, file):
        self.file = file
        self.root = ETree.parse(self.file).getroot()
        self.flag_dictionary = {}
        self.parse_dict()
        self.print_keys()

    def parse_dict(self):
        for elem in self.root:
            flag_name = elem.tag
            keywords = elem.text.replace(" ", ", ")
            self.flag_dictionary[flag_name] = keywords

    def items(self):
        return self.flag_dictionary.items()

    def keys(self):
        return self.flag_dictionary.keys()

    def values(self):
        return self.flag_dictionary.values()

    def __getitem__(self, key):
        return self.flag_dictionary[key]

    def __setitem__(self, key, value):
        self.flag_dictionary[key] = value

    def __delitem__(self, key):
        self.flag_dictionary.pop(key)

    def __contains__(self, item):
        return item in self.flag_dictionary

    def print_keys(self):
        for key, value in self.items():
            print(f"{key}: {value}")

    def get_default(self, key, default):
        if key not in self.flag_dictionary:
            return default
        return self[key]
        
    def get_flag_list(self):
        string = ""
        for key, value in self.items():
            string += f"{key.upper()}: {value}\n"
        return f"```\nFLAGS:\n\n{string}```"


if __name__ == '__main__':
    print('TESTING...')
    final = FlagDictionary('resources/flags.xml')
    print(final['gay'])
    print('\nTest run complete!')


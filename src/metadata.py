import xml.etree.ElementTree as ETree
import os


class Metadata:
    def __init__(self, file):
        self.file = file
        self.root = ETree.parse(self.file).getroot()
        self.prefix = ""
        self.main_token = ""
        self.test_token = ""
        self.description = ""
        self.working_dir = ""
        self.master = ""
        self.flags = ""
        self.test_mode = False

    def get_bot(self):
        self.main_token = self.root.find('main_token').text
        self.test_token = self.root.find('test_token').text
        self.prefix = self.root.find('prefix').text
        self.working_dir = self.root.find('working_dir').text
        if self.working_dir == 'default':
            self.working_dir = os.getcwd()
        self.test_mode = self.root.find('test_mode').text.lower() == "true"
        self.master = self.root.find('master').text
        self.description = self.root.find('description').text.format(self.master)
        self.flags = self.root.find('flags').text
        
    def get_item(self, item):
        return self.root.find(item).text


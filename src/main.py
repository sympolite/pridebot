#!usr/bin/env python3.6

# =============================================================
# PRIDE BOT 2.0
# by sympolite
# github.com/sympolite
# =============================================================

# core modules
import time
from datetime import datetime
import os
import sys
from discord.ext.commands import Bot
from torgoutils import ANSI
import metadata

class PrideBot:
    def __init__(self):
        self.client_token = ""
        self.metadata = metadata.Metadata('resources/meta.xml')
        self.metadata.get_bot()
        self.working_dir = self.metadata.working_dir
        self.temp_path = os.path.join(self.working_dir, 'temp')
        self.temp_file = os.path.join(self.temp_path, 'temp.lock')
        self.ansi = ANSI()
        self.pride = Bot(
            command_prefix=self.metadata.prefix,
            pm_help=True,
            description=self.metadata.description
        )
        setattr(self.pride, 'metadata', self.metadata)
        setattr(self.pride, 'ansi', self.ansi)
        print(self.pride)
        self.cogs = [
            'cogs.text',
            'cogs.events'
        ]

    def setup(self, remove_temp_file=False):
        print("="*64)
        print("="*64)
        print(f"{self.ansi.bold}{self.ansi.color['yellow']}PRIDEBOT 3{self.ansi.clear}")
        print(f"Working directory is: {self.metadata.working_dir}")
        os.chdir(self.working_dir)
        print("Working directory is: " + os.getcwd())
        if not os.path.exists(self.temp_path):
            os.mkdir(self.temp_path)
            print("Temp path created in: " + self.temp_path)
        else:
            print("Temp path exists at: " + self.temp_path)
        if remove_temp_file and os.path.exists(self.temp_file):
            os.remove(self.temp_file)
        elif not os.path.exists(self.temp_file):
            print("Creating lock file: " + self.temp_file)
            open(self.temp_file, 'a').close()
            print("Lock file created.")
            self.client_token = self.metadata.test_token if self.metadata.test_mode else self.metadata.main_token
            print(f"Using token: {self.client_token}.\nTest mode is: {self.metadata.test_mode}")
            print("Loading cogs...")
            for counter, cog in enumerate(self.cogs, 1):
                self.pride.load_extension(cog)
                print(f"*  Loaded cog {cog} ({counter} of {len(self.cogs)}).")
            print("Loaded all cogs.")
            print("Starting TORGO...")
            self.run()
        else:
            now = datetime.today().strftime("%B %d %Y, at %I:%M:%S %p")
            print("_" * 64)
            print(f"{now}:\n{self.ansi.color['red']}FATAL ERROR{self.ansi.clear}: "
                  f"Exiting with code 10: Lock file exists!")
            print(f"*  Is there an instance running?")
            print(f"*  If not, was the lock file not deleted?")
            print("=" * 64)
            print("=" * 64)
            exit(10)

    def run(self):
        try:
            self.pride.run(self.client_token)
        except Exception as e:
            exit_code = 1
            now = datetime.today().strftime("%B %d %Y, at %I:%M:%S %p")
            print("_"*64)
            print(f"{now}:\n{self.ansi.color['red']}FATAL ERROR{self.ansi.clear}: Exiting with code {exit_code}: "
                  f"An exception caused the bot to close. Info:")
            print("*  Type: " + str(type(e)))
            print("*  Message: " + repr(e))
            print("_"*64)
            print("_"*64)
            self.pride.close()
            print("Cleaning up temp files...")
            for file in os.listdir(self.temp_path):
                os.remove(os.path.join(self.temp_path, file))
            os.removedirs(self.temp_path)
            time.sleep(0.5)
            print("Cleanup complete. Program has closed.")
            print("_"*64)
            print("="*64)
            print("="*64)
            exit(exit_code)


if __name__ == "__main__":
    PRIDE = PrideBot()
    delete_temp_lock = 'true' in sys.argv
    PRIDE.setup(delete_temp_lock)
    PRIDE.run()



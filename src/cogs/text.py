import discord
from discord.ext import commands
from PIL import Image, ImageDraw, ImageFont
import torgoutils
import asyncio
import os
import metadata
from dictbuilder import *
import numpy as np


class Text(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.metadata = metadata.Metadata('resources/meta.xml')
        self.metadata.get_bot()
        self.working_dir = self.metadata.working_dir
        self.flag_dict = FlagDictionary(self.metadata.flags)
        self.flag_list = FlagList(self.metadata.flags)

    def verify_attachment(self, attachment, file_type):
        if attachment is None:
            return False
        if not attachment.filename.endswith(file_type):
            return False
        return True

    def autocrop(self, base_image):
        base_image_data = np.asarray(base_image)
        # get alpha channel of all pixels (R = 0, G = 1, B = 2, A = 3)

        image_data_bw = base_image_data[:, :, 3]
        # get all non-transparent pixels (alpha > 0)

        non_empty_columns = np.where(image_data_bw.max(axis=0) > 0)[0]
        non_empty_rows = np.where(image_data_bw.max(axis=1) > 0)[0]
        if not (non_empty_columns.any() and non_empty_rows.any()):
            return None
        crop_box = (min(non_empty_rows), max(non_empty_rows), min(non_empty_columns), max(non_empty_columns))
        # crop the image proper
        im_data = base_image_data[crop_box[0]:crop_box[1] + 1, crop_box[2]:crop_box[3] + 1, :]
        autocropped_image = Image.fromarray(im_data)
        return autocropped_image

    @commands.command(
        name="list",
        description="Lists all pride flags.",
        brief="Lists all pride flags.",
        usage="list"
    )
    async def list(self, ctx):
        await ctx.send(self.flag_list.get_flag_list())


    @commands.command(
        name="pride",
        description="Creates a pride flag, given an image and a flag name.",
        brief="Creates a pride flag!",
        usage="flagname [attached_file.png]"
    )
    async def pride(self, ctx, flag):
        if not flag:
            await ctx.send(f"ERROR: A flag name is needed. Try `<3list` for a list of flags.")
            return
        flag = flag.lower()
        if flag == "help":
            appendd = " Did you mean `<3help`?"
        if flag not in self.flag_dict:
            await ctx.send(f"ERROR: `{flag}` is currently not a valid pride flag name." + appendd)
            return
        if not os.path.exists(os.path.join("resources/img", self.flag_dict[flag]+".png")):
            await ctx.send(f"ERROR: `{flag}` does not have an associated image.")
            return

        attachments = ctx.message.attachments
        if not attachments:
            await ctx.send(f"ERROR: No image is attached or linked!")
            return
        else:
            url = attachments[0].url
        """
        elif not link:
            attachment = attachments[0]
            if not self.verify_attachment(attachment, 'png'):
                await ctx.send(f"ERROR: Image is not a png file!")
                return
            else:
                url = attachment.url
        else:
            url = link
        """
        async with ctx.typing():
            # and now... we save the image
            temp_image = torgoutils.save_image(url, '.png')
            flagg = self.flag_dict.get_default(flag, "error")
            base = Image.open(f'resources/img/{flagg}.png').convert('RGBA')
            top = Image.open(temp_image).convert('RGBA')
            copy_base = base.copy()
            copy_top = top.copy()
            # crop the image
            cropped_top = self.autocrop(copy_top)
            width, height = cropped_top.size
            flag_width, flag_height = copy_base.size
            min_dim = min(flag_width / width, flag_height / height)
            new_w = int(width * min_dim)
            new_h = int(height * min_dim)
            copy_top_resized = cropped_top.resize((new_w, new_h), resample=1)
            # put it on a blank image for alpha-compositing
            blank = Image.new('RGBA', (flag_width, flag_height), (0, 0, 0, 0))
            xpos = (flag_width - new_w) // 2
            ypos = (flag_height - new_h) // 2
            blank.paste(copy_top_resized, (xpos, ypos))
            # do the compositing
            final_image = Image.alpha_composite(copy_base, blank)
            final_image = final_image.resize((512, 512), resample=1)
            final_filepath = torgoutils.make_temp_file_name('.png')
            final_image.save(final_filepath, "PNG")
            await ctx.send(content=None, file=discord.File(final_filepath))
            await asyncio.sleep(0.5)
            # delete the temp files
            os.remove(os.path.join(os.getcwd(), final_filepath))
            os.remove(os.path.join(os.getcwd(), temp_image))


def setup(bot):
    bot.add_cog(Text(bot))

import discord
from discord.ext import commands
from datetime import datetime
from torgoutils import ANSI


class Events(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.ansi = self.bot.ansi
        self.in_call_role = "In Call"

    @commands.Cog.listener()
    async def on_ready(self):
        now = datetime.today().strftime("%B %d %Y, at %I:%M:%S %p")
        print(f"{now}:\n{self.ansi.color['green']}PrideBot is ready!{self.ansi.clear}")
        print("="*64)
        print("="*64)
        await self.bot.change_presence(
            status=discord.Status.online,
            activity=discord.Game(name=f'{self.bot.command_prefix}help')
        )

    @commands.Cog.listener()
    async def on_command(self, ctx):
        now = datetime.today().strftime("%B %d %Y, at %I:%M:%S %p")
        print("_" * 64)
        print(f"{now}:\nCommand ${ctx.invoked_with} CALLED. Info:")
        print(f"*  Author:    {ctx.author.display_name} ({ctx.author.name}#{ctx.author.discriminator})")
        print(f"*  Channel:   #{ctx.channel.name}")
        print(f"*  Guild:     {ctx.guild.name} ({ctx.guild.id})")
        print(f"*  Content:   {ctx.message.content}")
        print("_" * 64)

    @commands.Cog.listener()
    async def on_command_completion(self, ctx):
        now = datetime.today().strftime("%B %d %Y, at %I:%M:%S %p")
        print("_" * 64)
        print(f"{now}:\nCommand ${ctx.invoked_with} {self.ansi.color['green']}COMPLETED.{self.ansi.clear} Info:")
        print(f"*  Author:    {ctx.author.display_name} ({ctx.author.name}#{ctx.author.discriminator})")
        print(f"*  Channel:   #{ctx.channel.name}")
        print(f"*  Guild:     {ctx.guild.name} ({ctx.guild.id})")
        print(f"*  Content:   {ctx.message.content}")
        print("_" * 64)

    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        if isinstance(error, commands.CommandNotFound):
            await ctx.send(f"ERROR: `${ctx.invoked_with}` is not a command I recognize. "
                           f"Use `$help` to show a list of commands.")
        now = datetime.today().strftime("%B %d %Y, at %I:%M:%S %p")
        print("_" * 64)
        print(f"{now}:\nCommand ${ctx.invoked_with} {self.ansi.color['red']}FAILED.{self.ansi.clear} Info:")
        print(f"*  Author:    {ctx.author.display_name} ({ctx.author.name}#{ctx.author.discriminator})")
        print(f"*  Channel:   #{ctx.channel.name}")
        print(f"*  Guild:     {ctx.guild.name} ({ctx.guild.id})")
        print(f"*  Content:   {ctx.message.content}")
        print(f"*  Error:     {repr(error)}")
        print("_" * 64)


def setup(bot):
    bot.add_cog(Events(bot))

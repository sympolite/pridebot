# PrideBot

by tianders295

## Description

PrideBot is a bot for Discord that can generate pride flag icons (a character superimposed onto a pride flag), given a transparent PNG file and a flag name.

## Commands

All commands are prefixed by default with <3; this can be changed in the meta.xml file.

```
<3help
```
This sends a list of commands and their functions.

```
<3list                          
```
This lists all flags and the keywords associated with them (as specified in flags.xml).

```
<3pride flagname [attachment.png]  
```
This generates a pride flag icon, given a name and an attached (not linked) .PNG file.
If the flag name is "help," this command suggests you use the help command.

## Requirements

1. Python 3.6 or newer. This is important; the bot will NOT run on earlier versions.
2. discord.py, PIL, and NumPy. Once you have Python 3.6 installed, you can install them using the "pip" utility in your command line of choice:
```bash
pip3 install discord.py pil numpy
```

## Running The Bot

Using the command line, you can run the bot like so:
```bash
python3 main.py true
```
"true" denotes if the temp.lock file needs to be deleted - "true" is recommended if you're restarting the bot, while "false" is recommended if you're keeping the bot running.

## Maintaining The Bot

Because online LGBTQ+ communities tend to vary in what pride flags they do and don't use, there is a fully customizable XML file entitled "flags.xml" in the resources folder.
Likewise, the flags to be used are 1000x1000 PNGs stored in resources/img.

### Adding flags

To add a flag, use an image editor of your choice to make a 1000x1000 PNG of said flag, add it to resources/img, and add an entry like this in flags.xml *before* </flags>:
```xml
<your_flag>foo bar baz</your_flag>
```

### Temp.lock

The temp.lock file ensures that no more than one instance of the bot is running, and is generated when the bot starts up. This file can be deleted manually or by the method detailed in "Running the Bot."

## License

[MIT](https://choosealicense.com/licenses/mit/)
